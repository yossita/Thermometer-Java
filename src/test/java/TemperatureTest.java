package test.java;

import main.java.Celsius;
import main.java.Fahrenheit;
import main.java.Temperature;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureTest {
    @Test
    void equals() {
        Temperature t1 = new Celsius(25.456);
        Temperature t2 = new Celsius(25.456);
        assertEquals(t1, t2);
    }

    @Test
    void smallerThan() {
        Temperature t1 = new Celsius(25.456);
        Temperature t2 = new Celsius(344.2366);
        assertTrue(t1.smallerThan(t2));
    }

    @Test
    void greaterThan() {
        Temperature t1 = new Celsius(25.456);
        Temperature t2 = new Celsius(-212.01);
        assertTrue(t1.greaterThan(t2));
    }

    @Test
    void equals_FtoC() {
        Temperature t1 = new Celsius(0);
        Temperature t2 = new Fahrenheit(32);
        assertEquals(t1, t2);
    }

    @Test
    void equals_CtoF() {
        Temperature t1 = new Fahrenheit(0);
        Temperature t2 = new Celsius(-17.7778);
        assertEquals(t1, t2);
    }

    @Test
    void equals_FtoF() {
        Temperature t1 = new Fahrenheit(45.664);
        Temperature t2 = new Fahrenheit(45.664);
        assertEquals(t1, t2);
    }

    @Test
    void greaterThan_FtoC() {
        Temperature t1 = new Celsius(0);
        Temperature t2 = new Fahrenheit(31.9999);
        assertTrue(t1.greaterThan(t2));
    }

    @Test
    void smallerThan_CtoF() {
        Temperature t1 = new Fahrenheit(0);
        Temperature t2 = new Celsius(-17.7777);
        assertTrue(t1.smallerThan(t2));
    }

    @Test
    void equals_sameValueWithZeroAccuracy() {
        Temperature t1 = new Celsius(5);
        Temperature t2 = new Celsius(5);
        Temperature tolerance = new Celsius(0);
        assertTrue(t1.equals(t2, tolerance));
    }

    @Test
    void equals_sameValueWithAccuracy() {
        Temperature t1 = new Celsius(5);
        Temperature t2 = new Celsius(5);
        Temperature tolerance = new Celsius(1);
        assertTrue(t1.equals(t2, tolerance));
    }

    @Test
    void equals_withinTolerance() {
        Temperature t1 = new Celsius(5);
        Temperature t2 = new Celsius(6);
        Temperature tolerance = new Celsius(1);
        assertTrue(t1.equals(t2, tolerance));
    }

    @Test
    void equals_outsideTolerance() {
        Temperature t1 = new Celsius(5);
        Temperature t2 = new Celsius(7);
        Temperature tolerance = new Celsius(1);
        assertFalse(t1.equals(t2, tolerance));
    }

    @Test
    void smallerThan_withinTolerance() {
        Temperature t1 = new Celsius(5);
        Temperature t2 = new Celsius(6);
        Temperature tolerance = new Celsius(1);
        assertFalse(t1.smallerThan(t2, tolerance));
    }

    @Test
    void smallerThan_outsideTolerance() {
        Temperature t1 = new Celsius(5);
        Temperature t2 = new Celsius(7);
        Temperature tolerance = new Celsius(1);
        assertTrue(t1.smallerThan(t2, tolerance));
    }

    @Test
    void greaterThan_withinTolerance() {
        Temperature t1 = new Celsius(6);
        Temperature t2 = new Celsius(5);
        Temperature tolerance = new Celsius(1);
        assertFalse(t1.greaterThan(t2, tolerance));
    }

    @Test
    void greaterThan_outsideTolerance() {
        Temperature t1 = new Celsius(7);
        Temperature t2 = new Celsius(5);
        Temperature tolerance = new Celsius(1);
        assertTrue(t1.greaterThan(t2, tolerance));
    }
}