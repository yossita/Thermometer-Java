package test.java;

import main.java.ITemperatureSource;
import main.java.Temperature;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TestHarnessTemperatureSource implements ITemperatureSource {

    private ListIterator<Temperature> temperatureList;

    public TestHarnessTemperatureSource(List<Temperature> temperatureList) {

        this.temperatureList = temperatureList.listIterator();
    }

    @Override
    public Iterator<Temperature> iterator() {
        return temperatureList;
    }
}
