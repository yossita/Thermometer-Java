package test.java;

import main.java.IThresholdReachedListener;
import main.java.Temperature;
import main.java.Threshold;

import java.util.ArrayList;
import java.util.List;

public class TestHarnessThresholdReachedListener implements IThresholdReachedListener {

    public TestHarnessThresholdReachedListener() {
        reachedThresholds = new ArrayList<>();
    }

    public List<ReachedThreshold> getReachedThresholds() {
        return reachedThresholds;
    }

    public class ReachedThreshold {

        private final Threshold threshold;
        private final Temperature temperature;

        public ReachedThreshold(Threshold threshold, Temperature temperature) {
            this.threshold = threshold;
            this.temperature = temperature;
        }

        public Threshold getThreshold() {
            return threshold;
        }

        public Temperature getTemperature() {
            return temperature;
        }
    }

    private List<ReachedThreshold> reachedThresholds;

    @Override
    public void ThresholdReached(Threshold threshold, Temperature temperature) {
        reachedThresholds.add(new ReachedThreshold(threshold, temperature));
    }
}
