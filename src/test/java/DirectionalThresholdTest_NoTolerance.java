package test.java;

import main.java.Celsius;
import main.java.DirectionalThreshold;
import main.java.Temperature;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DirectionalThresholdTest_NoTolerance {

    @Test
    void wasReached_upTrendAndExcat_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), DirectionalThreshold.TrendDirection.Up);
        List<Temperature> readings = Arrays.asList(
                new Celsius(-5),
                new Celsius(-3),
                new Celsius(0),
                new Celsius(0.3),
                new Celsius(1.0)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }

    @Test
    void wasReached_upTrendAndAbove_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), DirectionalThreshold.TrendDirection.Up);
        List<Temperature> readings = Arrays.asList(
                new Celsius(-5),
                new Celsius(-3),
                new Celsius(0.5),
                new Celsius(0.7),
                new Celsius(1.0)
        );
        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }

    @Test
    void wasReached_downTrendAndExact_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), DirectionalThreshold.TrendDirection.Down);
        List<Temperature> readings = Arrays.asList(
                new Celsius(5),
                new Celsius(3),
                new Celsius(0),
                new Celsius(0.3),
                new Celsius(1.3)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }

    @Test
    void wasReached_downTrendAndBelow_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), DirectionalThreshold.TrendDirection.Down);
        List<Temperature> readings = Arrays.asList(
                new Celsius(5),
                new Celsius(3),
                new Celsius(-0.5),
                new Celsius(-0.7),
                new Celsius(-1.1)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }

    @Test
    void wasReached_Below_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0));
        List<Temperature> readings = Arrays.asList(
                new Celsius(5),
                new Celsius(3),
                new Celsius(-0.5),
                new Celsius(-0.6),
                new Celsius(-0.2),
                new Celsius(1)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
        assertTrue(threshold.wasReached(readings.get(5)));
    }

    @Test
    void wasReached_SameTemperatureReachedTwice_reachedOnlyOncePerReach() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0));
        List<Temperature> readings = Arrays.asList(
                new Celsius(5),
                new Celsius(0),
                new Celsius(0),
                new Celsius(0),
                new Celsius(1),
                new Celsius(0),
                new Celsius(0)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertTrue(threshold.wasReached(readings.get(1)));
        assertFalse(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
        assertTrue(threshold.wasReached(readings.get(5)));
        assertFalse(threshold.wasReached(readings.get(6)));
    }
}