package test.java;

import main.java.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThermometerTest {

    @Test
    void Start_SampleTemps_ReadAll() {
        List<Temperature> testTemps = Arrays.asList(
                new Celsius(14),
                new Celsius(15),
                new Celsius(16),
                new Celsius(17),
                new Celsius(18),
                new Celsius(19),
                new Celsius(20)
        );
        TestHarnessTemperatureSource testHarnessSource = new TestHarnessTemperatureSource(testTemps);
        List<Threshold> thresholds = new ArrayList<>();
        TestHarnessThresholdReachedListener testListener = new TestHarnessThresholdReachedListener();
        Thermometer thermometer = new Thermometer(testHarnessSource, testListener, thresholds);
        thermometer.start();
        assertEquals(7, thermometer.getNumberOfReadings());
    }

    @Test
    void Start_ThresholdReachedOnce_ListenerCalledOnce() {
        List<Temperature> testTemps = Arrays.asList(
                new Celsius(14),
                new Celsius(10),
                new Celsius(16)
        );
        TestHarnessTemperatureSource testHarnessSource = new TestHarnessTemperatureSource(testTemps);
        List<Threshold> thresholds = Arrays.asList(new SimpleExactTemperatureThreshold("test", new Celsius(10)));
        TestHarnessThresholdReachedListener testListener = new TestHarnessThresholdReachedListener();

        Thermometer thermometer = new Thermometer(testHarnessSource, testListener, thresholds);
        thermometer.start();

        assertEquals(1, testListener.getReachedThresholds().size());
    }

    @Test
    void Start_ThresholdReachedTwice_ListenerCalledTwice() {
        List<Temperature> testTemps = Arrays.asList(
                new Celsius(14),
                new Celsius(10),
                new Celsius(16),
                new Celsius(10)
        );
        TestHarnessTemperatureSource testHarnessSource = new TestHarnessTemperatureSource(testTemps);
        List<Threshold> thresholds = Arrays.asList(new SimpleExactTemperatureThreshold("test", new Celsius(10)));
        TestHarnessThresholdReachedListener testListener = new TestHarnessThresholdReachedListener();

        Thermometer thermometer = new Thermometer(testHarnessSource, testListener, thresholds);
        thermometer.start();

        assertEquals(2, testListener.getReachedThresholds().size());
    }
}