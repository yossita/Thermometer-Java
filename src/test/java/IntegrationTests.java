package test.java;

import main.java.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationTests {

    @Test
    void multipleThresholdsTest() {
        List<Temperature> testTemps = Arrays.asList(
                new Celsius(14),
                new Celsius(10),//1
                new Celsius(16),
                new Celsius(10),//2
                new Celsius(-5),//3
                new Celsius(-1),
                new Celsius(2),//4
                new Celsius(-1),//5
                new Celsius(94),//6
                new Celsius(95),//7
                new Celsius(96),
                new Celsius(101),
                new Celsius(99),
                new Celsius(102),
                new Celsius(93),
                new Celsius(97)//8
        );
        TestHarnessTemperatureSource testHarnessSource = new TestHarnessTemperatureSource(testTemps);
        List<Threshold> thresholds = Arrays.asList(
                new SimpleExactTemperatureThreshold("simple at 10C", new Celsius(10)),
                new DirectionalThreshold("freeze at 32F no fluctuation", new Fahrenheit(32)),
                new DirectionalThreshold("Boiling at 100C with 5C fluctuation, rising only",
                        new Celsius(100),
                        DirectionalThreshold.TrendDirection.Up,
                        new Celsius(5))
        );
        TestHarnessThresholdReachedListener testListener = new TestHarnessThresholdReachedListener();

        Thermometer thermometer = new Thermometer(testHarnessSource, testListener, thresholds);
        thermometer.start();

        assertEquals(8, testListener.getReachedThresholds().size());
    }
}
