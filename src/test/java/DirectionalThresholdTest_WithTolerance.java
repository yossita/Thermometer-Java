package test.java;

import main.java.Celsius;
import main.java.DirectionalThreshold;
import main.java.Temperature;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DirectionalThresholdTest_WithTolerance {

    @Test
    void wasReached_upTrendWithinTolerance_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), DirectionalThreshold.TrendDirection.Up, new Celsius(0.1));
        List<Temperature> readings = Arrays.asList(
                new Celsius(-5),
                new Celsius(-3),
                new Celsius(-0.05),
                new Celsius(0.3),
                new Celsius(1.0)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }

    @Test
    void wasReached_downTrendWithinTolerance_reached() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), DirectionalThreshold.TrendDirection.Down, new Celsius(0.1));
        List<Temperature> readings = Arrays.asList(
                new Celsius(5),
                new Celsius(3),
                new Celsius(0.05),
                new Celsius(-0.3),
                new Celsius(-1.0)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertFalse(threshold.wasReached(readings.get(1)));
        assertTrue(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }

    @Test
    void wasReached_FluctuateWithinTolerance_reachedOnlyOnce() {
        DirectionalThreshold threshold = new DirectionalThreshold("test", new Celsius(0), new Celsius(0.1));
        List<Temperature> readings = Arrays.asList(
                new Celsius(5),
                new Celsius(0.05),
                new Celsius(-0.03),
                new Celsius(0.05),
                new Celsius(-1.0)
        );

        assertFalse(threshold.wasReached(readings.get(0)));
        assertTrue(threshold.wasReached(readings.get(1)));
        assertFalse(threshold.wasReached(readings.get(2)));
        assertFalse(threshold.wasReached(readings.get(3)));
        assertFalse(threshold.wasReached(readings.get(4)));
    }
}
