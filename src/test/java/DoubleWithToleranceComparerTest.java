package test.java;

import main.java.DoubleWithToleranceComparer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DoubleWithToleranceComparerTest {

    @Test
    public void compare_SamePositiveTemperatureNoTolerance_Zero() {
        double t1 = 10;
        double t2 = 10;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2));
    }

    @Test
    public void compare_SameNegativeTemperatureNoTolerance_Zero() {
        double t1 = -10;
        double t2 = -10;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2));
    }

    @Test
    public void compare_SamePositiveTemperatureWithTolerance_Zero() {
        double t1 = 10;
        double t2 = 10;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_SameNegativeTemperatureWithTolerance_Zero() {
        double t1 = -10;
        double t2 = -10;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_PositiveTemperatureEdgeTolerance_Zero() {
        double t1 = 10;
        double t2 = 10.5;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_PositiveTemperatureAboveWithinTolerance_Zero() {
        double t1 = 10;
        double t2 = 10.4;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_PositiveTemperatureBelowWithinTolerance_Zero() {
        double t1 = 10;
        double t2 = 9.6;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_PositiveTemperatureAboveTolerance_Negative() {
        double t1 = 10;
        double t2 = 10.6;
        double tolerance = 0.5;

        assertTrue(DoubleWithToleranceComparer.compare(t1, t2, tolerance) < 0);
    }

    @Test
    public void compare_PositiveTemperatureBelowTolerance_Positive() {
        double t1 = 10;
        double t2 = 9.4;
        double tolerance = 0.5;

        assertTrue(DoubleWithToleranceComparer.compare(t1, t2, tolerance) > 0);
    }

    @Test
    public void compare_NegativeTemperatureEdgeTolerance_Zero() {
        double t1 = -10;
        double t2 = -10.5;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_NegativeTemperatureAboveWithinTolerance_Zero() {
        double t1 = -10;
        double t2 = -10.4;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_NegativeTemperatureBelowWithinTolerance_Zero() {
        double t1 = -10;
        double t2 = -9.6;
        double tolerance = 0.5;

        assertEquals(0, DoubleWithToleranceComparer.compare(t1, t2, tolerance));
    }

    @Test
    public void compare_NegativeTemperatureAboveTolerance_Negative() {
        double t1 = -10;
        double t2 = -9.4;
        double tolerance = 0.5;

        assertTrue(DoubleWithToleranceComparer.compare(t1, t2, tolerance) < 0);
    }

    @Test
    public void compare_NegativeTemperatureBelowTolerance_Positive() {
        double t1 = -10;
        double t2 = -10.6;
        double tolerance = 0.5;

        assertTrue(DoubleWithToleranceComparer.compare(t1, t2, tolerance) > 0);
    }
}