package test.java;

import main.java.Celsius;
import main.java.SimpleExactTemperatureThreshold;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SimpleExactTemperatureThresholdTest {

    @Test
    void wasReached_sameTemperature_reached() {
        SimpleExactTemperatureThreshold threshold = new SimpleExactTemperatureThreshold("test", new Celsius(10));
        assertTrue(threshold.wasReached(new Celsius(10)));
    }

    @Test
    void wasReached_differentTemperature_notReached() {
        SimpleExactTemperatureThreshold threshold = new SimpleExactTemperatureThreshold("test", new Celsius(10));
        assertFalse(threshold.wasReached(new Celsius(10.0001)));
    }
}