package main.java;

public class Celsius extends Temperature {

    public Celsius(double value) {
        super(value);
    }

    @Override
    protected Temperature convertToCelsius() {
        return this;
    }


}
