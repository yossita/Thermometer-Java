package main.java;

public class DoubleWithToleranceComparer {

    public static int compare(double one, double another) {
        return compare(one, another, 0);
    }

    public static int compare(double one, double another, double tolerance) {
        tolerance = Math.abs(tolerance);
        double upperBound = one + tolerance;
        double lowerBound = one - tolerance;

        if (Double.compare(upperBound, another) >= 0 && Double.compare(lowerBound, another) <= 0) return 0;
        return Double.compare(one, another);
    }
}
