package main.java;

public class SimpleExactTemperatureThreshold extends Threshold {

    protected final Temperature threshold;

    public SimpleExactTemperatureThreshold(String description, Temperature threshold) {
        super(description);
        this.threshold = threshold;
    }

    @Override
    public boolean wasReached(Temperature currentReading) {
        return currentReading.equals(threshold);
    }
}
