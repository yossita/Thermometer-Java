package main.java;

import java.math.BigDecimal;

public class Fahrenheit extends Temperature {

    private static final int precision = 4;

    public Fahrenheit(double value) {
        super(value);
    }

    @Override
    protected Temperature convertToCelsius() {
        Double convertedValue = (this.value - 32) / 1.8;
        convertedValue = BigDecimal.valueOf(convertedValue).setScale(precision, BigDecimal.ROUND_HALF_DOWN).doubleValue();
        return new Celsius(convertedValue);
    }
}
