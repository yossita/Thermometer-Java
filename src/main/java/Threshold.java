package main.java;

public abstract class Threshold {

    public String getDescription() {
        return description;
    }

    private final String description;

    public Threshold(String description) {
        this.description = description;
    }

    public abstract boolean wasReached(Temperature currentReading);
}
