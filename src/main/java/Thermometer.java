package main.java;

import java.util.List;

public class Thermometer {

    private final IThresholdReachedListener notificationListener;
    private List<Threshold> thresholds;
    private ITemperatureSource source;
    private int readCount;

    public int getNumberOfReadings() {
        return readCount;
    }

    public Thermometer(ITemperatureSource source, IThresholdReachedListener notificationListener, List<Threshold> thresholds) {
        this.source = source;
        this.notificationListener = notificationListener;
        this.thresholds = thresholds;
        readCount = 0;
    }

    public void start() {
        source.forEach(t -> {
            System.out.println("Read temperature " + t.toString());
            thresholds.forEach(th -> {
                if (th.wasReached(t)) {
                    System.out.println("Threshold " + th.getDescription() + " reached");
                    notificationListener.ThresholdReached(th, t);
                }
            });
            readCount++;
        });
    }

}
