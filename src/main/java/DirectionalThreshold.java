package main.java;

public class DirectionalThreshold extends Threshold {

    private final Temperature temperature;
    private final TrendDirection trendDirection;
    private final Temperature allowedFluctuation;
    private Temperature lastReading;

    public enum TrendDirection {
        Both,
        Up,
        Down
    }

    public DirectionalThreshold(String description, Temperature temperature) {
        this(description, temperature, TrendDirection.Both, Temperature.ZERO);
    }

    public DirectionalThreshold(String description, Temperature temperature, Temperature allowedFluctuation) {
        this(description, temperature, TrendDirection.Both, allowedFluctuation);
    }

    public DirectionalThreshold(String description, Temperature temperature, TrendDirection trendDirection) {
        this(description, temperature, trendDirection, Temperature.ZERO);
    }

    public DirectionalThreshold(String description, Temperature temperature, TrendDirection trendDirection, Temperature allowedFluctuation) {
        super(description);
        this.temperature = temperature;
        this.trendDirection = trendDirection;
        this.allowedFluctuation = allowedFluctuation;
    }

    @Override
    public boolean wasReached(Temperature currentReading) {
        if (lastReading == null) {
            lastReading = currentReading;
            return currentReading.equals(temperature);
        }

        boolean reached = crossedThresholdTemperature(currentReading);
        lastReading = currentReading;
        return reached;
    }

    private boolean crossedThresholdTemperature(Temperature currentReading) {
        if (trendDirection == TrendDirection.Up && isTrendingUp(currentReading))
            return isCrossedUpwards(currentReading);
        else if (trendDirection == TrendDirection.Down && isTrendingDown(currentReading))
            return isCrossedDownwards(currentReading);
        else
            return (isCrossedDownwards(currentReading)) || (isCrossedUpwards(currentReading));
    }

    private boolean isCrossedDownwards(Temperature currentReading) {
        return lastReading.greaterThan(temperature, allowedFluctuation) && currentReading.smallerThanOrEquals(temperature, allowedFluctuation);
    }

    private boolean isCrossedUpwards(Temperature currentReading) {
        return lastReading.smallerThan(temperature, allowedFluctuation) && currentReading.greaterThanOrEquals(temperature, allowedFluctuation);
    }

    private boolean isTrendingDown(Temperature currentReading) {
        return lastReading.greaterThan(currentReading);
    }

    private boolean isTrendingUp(Temperature currentReading) {
        return lastReading.smallerThan(currentReading);
    }
}
