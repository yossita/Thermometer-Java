package main.java;

public abstract class Temperature {

    public static final Temperature ZERO = new Celsius(0);

    protected final double value;

    public Temperature(double value) {
        this.value = value;
    }

    protected abstract Temperature convertToCelsius();

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                "{" +
                "value=" + value +
                '}';
    }

    private int compareTo(Temperature that, Temperature tolerance) {
        double t1 = this.convertToCelsius().value;
        double t2 = that.convertToCelsius().value;
        double allowedTolerance = tolerance.convertToCelsius().value;
        return DoubleWithToleranceComparer.compare(t1, t2, allowedTolerance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Temperature)) return false;

        Temperature that = (Temperature) o;

        return equals(that);
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(convertToCelsius().value);
        return (int) (temp ^ (temp >>> 32));
    }

    public boolean equals(Temperature that) {
        return equals(that, Temperature.ZERO);
    }

    public boolean equals(Temperature that, Temperature accuracy) {
        return compareTo(that, accuracy) == 0;
    }

    public boolean smallerThan(Temperature that) {
        return smallerThan(that, Temperature.ZERO);
    }

    public boolean smallerThan(Temperature that, Temperature accuracy) {
        return compareTo(that, accuracy) < 0;
    }

    public boolean greaterThan(Temperature that) {
        return greaterThan(that, Temperature.ZERO);
    }

    public boolean greaterThan(Temperature that, Temperature accuracy) {
        return compareTo(that, accuracy) > 0;
    }

    public boolean smallerThanOrEquals(Temperature that) {
        return smallerThanOrEquals(that, Temperature.ZERO);
    }

    public boolean smallerThanOrEquals(Temperature that, Temperature accuracy) {
        return compareTo(that, accuracy) <= 0;
    }

    public boolean greaterThanOrEquals(Temperature that) {
        return greaterThanOrEquals(that, Temperature.ZERO);
    }

    public boolean greaterThanOrEquals(Temperature that, Temperature accuracy) {
        return compareTo(that, accuracy) >= 0;
    }
}
