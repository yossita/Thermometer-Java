package main.java;

public interface IThresholdReachedListener {
    void ThresholdReached(Threshold threshold, Temperature temperature);
}
